//
// Created by thoresen on 9/17/15.
//

#include <RooFFTConvPdf.h>
#include <RooGaussian.h>
#include "play.h"
#include "RooRealVar.h"

    play::play() {
        RooRealVar x("x","x",-10,10);
        RooRealVar mean("mean","mean of gaussian",1,-10,10) ;
        RooRealVar sigma("sigma","width of gaussian",1,0.1,10) ;

        RooRealVar mean1("mean","mean of gaussian",2,-10,10) ;
        RooRealVar sigma1("sigma","width of gaussian",2,0.1,10) ;

        RooRealVar mean2("mean","mean of gaussian",3,-10,10) ;
        RooRealVar sigma2("sigma","width of gaussian",3,0.1,10) ;

        RooGaussian gauss("gauss","gaussian PDF",x,mean,sigma) ;
        RooGaussian gauss1("gauss1","gaussian PDF",x,mean1,sigma1) ;
        RooGaussian gauss2("gauss2","gaussian PDF",x,mean2,sigma2) ;

        RooFFTConvPdf first("model","model",x,gauss1,gauss);

        HHiggs = new RooFFTConvPdf("model","model",x,first,gauss2);
    }

    RooFFTConvPdf* play::getHiggs() {
        return HHiggs;
    }


