//////////////////////////////////////////////////////////////////////////
//
// Freja Thoresen
//
/////////////////////////////////////////////////////////////////////////
#include "TF3.h"
#include "RooWorkspace.h"
#include <RooNumConvPdf.h>
#include <RooFFTConvPdf.h>
#include <RooExponential.h>
#include <RooPolyVar.h>
#include <RooPolynomial.h>
#include "Fit/BinData.h"
#include "Fit/Fitter.h"
#include "Math/WrappedMultiTF1.h"
#include <play.h>
#include <RooAddPdf.h>
#include <TLegend.h>
//#include "atlassstyle/"
#include "atlasstyle/AtlasUtils.h"
#include "atlasstyle/AtlasStyle.h"
#include "atlasstyle/AtlasLabels.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "TCanvas.h"
#include "TStyle.h"

#include "RooPlot.h"
#include "TAxis.h"
#include "TH2D.h"
#include "TROOT.h"
#include "RooProdPdf.h"
#include "TFile.h"
#include "RooGenericPdf.h"
using namespace RooFit ;
using namespace std ;


void HiggsConv(RooWorkspace *ws, TString name) {
    RooRealVar x = *ws->var("x");

    RooRealVar mean(name+"_mean","mean of gaussian",1,-10,10);
    RooRealVar sigma(name+"_sigma","width of gaussian",1,0.1,10);

    RooRealVar mean1(name+"_mean","mean of gaussian",2,-10,10) ;
    RooRealVar sigma1(name+"_sigma","width of gaussian",2,0.1,10) ;

    RooRealVar mean2(name+"_mean","mean of gaussian",3,-10,10) ;
    RooRealVar sigma2(name+"_sigma","width of gaussian",3,0.1,10) ;

    RooGaussian gauss(name+"_gauss","gaussian PDF",x,mean,sigma) ;
    RooGaussian gauss1(name+"_gauss1","gaussian PDF",x,mean1,sigma1) ;
    RooGaussian gauss2(name+"_gauss2","gaussian PDF",x,mean2,sigma2);

    RooRealVar frac("frac","fraction",0.5,0.0,1.0);
    RooAddPdf H_model("H_model","H_model",RooArgList(gauss,gauss1,gauss2),RooArgList(frac,frac));

    //RooGenericPdf Higgs("Higgs","Higgs",HiggsVar);
    //RooFFTConvPdf first(name+"_first",name+"_first",x,gauss1,gauss);

    //RooFFTConvPdf Higgs(name+"_model",name+"_model",x,first,gauss2);
    H_model.Print("v");

    ws->import( H_model );
}

void ZZConv(RooWorkspace *ws, TString name) {
    RooRealVar y = *ws->var("y");

    RooRealVar mean(name+"_"+"mean","mean of gaussian",1,-10,10) ;
    RooRealVar sigma(name+"_"+"sigma","width of gaussian",1,0.1,10) ;

    RooRealVar mean1(name+"mean","mean of gaussian",1,-10,10) ;
    RooRealVar sigma1(name+"sigma","width of gaussian",1,0.1,10) ;

    RooRealVar tau(name+"_"+"tau", "tau",-1.54,-10,-0.1);

    RooExponential expo(name+"_expo",name+"_expo",y,tau);
    RooGaussian gauss(name+"_gauss","gaussian PDF",y,mean,sigma) ;
    RooGaussian gauss1(name+"_gauss1","gaussian PDF",y,mean1,sigma1) ;

    RooRealVar frac("frac","fraction",0.5,0.0,1.0);
    RooAddPdf ZZ("ZZ_model","ZZ_model",RooArgList(expo,gauss,gauss1),RooArgList(frac,frac));

    ws->import(ZZ);
}

void jjConv(RooWorkspace *ws, TString name) {
    RooRealVar z = *ws->var("z");

    RooRealVar mean(name+"_"+"mean","mean of gaussian",1,-10,10) ;
    RooRealVar sigma(name+"_"+"sigma","width of gaussian",1,0.1,10) ;

    // make background function
    RooRealVar a0(name+"_a0","a0",-0.5,-5,5) ;
    RooRealVar p1(name+"_p1","coeff #1", 1, -1., 10.);
    RooRealVar p2(name+"_p2","coeff #2", 1, -1., 10.);
    RooRealVar p3(name+"_p3","coeff #3", 1, -1., 10.);
    RooRealVar p4(name+"_p4","coeff #4", 1, -1., 10.);
    RooRealVar p5(name+"_p5","coeff #5", 1, -1., 10.);

    RooPolynomial poly(name+"_poly","poly",z,RooArgList(p1,p2,p3,p4,p5)) ;

    RooExponential expo(name+"_expo",name+"_expo",z,a0);
    RooGaussian gauss(name+"_gauss","gaussian PDF",z,mean,sigma) ;


    RooRealVar frac("frac","fraction",0.5,0.0,1.0);
    RooAddPdf jj("jj_model","jj_model",RooArgList(gauss,poly,expo),RooArgList(frac,frac));


    //RooFFTConvPdf first(name+"_first","first",x,poly,gauss);
    //RooFFTConvPdf jj(name+"_model",name+"_model",x,first,expo);

    ws->import(jj);
}

int main()
{
   gROOT->LoadMacro("atlassyle/AtlasUtils.C");
   SetAtlasStyle();


    // make workspace
    RooWorkspace *work = new RooWorkspace("combined");
    RooRealVar x("x","x",-10,10);
    x.setBins(40);
    RooRealVar y(("y"),("y"),-10,10);
    y.setBins(40,"cache");
    RooRealVar z(("z"),("z"),-10,10);
    z.setBins(40,"cache");
    work->import(x);
    work->import(y);
    work->import(z);

    // import into workspace
    HiggsConv(work, "H");
    ZZConv(work, "ZZ");
    jjConv(work,"jj");

    // Construct plot frame in 'x'

    RooAddPdf *ZZ = (RooAddPdf*)work->pdf("ZZ_model");
    RooAddPdf *Higgs = (RooAddPdf*)work->pdf("H_model");
    RooAddPdf *jj = (RooAddPdf*)work->pdf("jj_model");


    RooProdPdf total("total","total",RooArgList(*ZZ,*Higgs,*jj));


    RooDataSet *xx = (*Higgs).generate(x,100);
    RooDataSet *yy = (*ZZ).generate(y,100);
    RooDataSet *zz = (*jj).generate(z,100);


    // --------------------------------- WEIGHT FOR PLOT --------------------------------------------------------------
    // Construct formula to calculate (fake) weight for events
    //RooFormulaVar wFunc("w","event weight","(x*x+10)",x) ;

    // Add column with variable w to previously generated dataset
    //RooRealVar* w = (RooRealVar*) xx->addColumn(wFunc) ;

    // Dataset d is now a dataset with two observable (x,w) with 1000 entries
    //xx->Print() ;

    // Instruct dataset wdata in interpret w as event weight rather than as observable
    //RooDataSet w_xx(xx->GetName(),xx->GetTitle(),xx,*xx->get(),0,w->GetName()) ;

    // Dataset d is now a dataset with one observable (x) with 1000 entries and a sum of weights of ~430K
    //w_xx.Print();
// ------------------------------------ XX FIT ---------------------------------------------------------------------
    TString name = "Higgsfit";
    RooRealVar mean(name+"_mean","mean of gaussian",1,-10,10);
    RooRealVar sigma(name+"_sigma","width of gaussian",1,0.1,10);
    RooGaussian gauss(name+"_gauss","gaussian PDF",x,mean,sigma) ;

    RooFitResult* xx_fit = gauss.fitTo(*xx,Save()) ;
    RooFitResult* xx_fit_corr = gauss.fitTo(*xx,Save(),SumW2Error(kTRUE)) ;

    RooPlot* xframe = x.frame(Title("Unbinned ML fit, binned chi^2 fit to weighted data")) ;

    // Plot data using sum-of-weights-squared error rather than Poisson errors
    (*xx).plotOn(xframe,DataError(RooAbsData::SumW2)) ;

    // Overlay result of 2nd order polynomial fit to weighted data
    gauss.plotOn(xframe) ;



    // ------------------------------------ YY FIT ---------------------------------------------------------------------
    name = "ZZfit";
    RooRealVar mean1(name+"_mean","mean of gaussian",1,-10,10);
    RooRealVar sigma1(name+"_sigma","width of gaussian",1,0.1,10);
    RooGaussian gauss1(name+"_gauss","gaussian PDF",y,mean,sigma) ;

    RooFitResult* yy_fit = gauss1.fitTo(*yy,Save()) ;
    RooFitResult* yy_fit_corr = gauss1.fitTo(*yy,Save(),SumW2Error(kTRUE)) ;

    RooPlot* yframe = y.frame(Title("Unbinned ML fit, binned chi^2 fit to weighted data")) ;

    // Plot data using sum-of-weights-squared error rather than Poisson errors
    (*yy).plotOn(yframe,DataError(RooAbsData::SumW2)) ;

    // Overlay result of 2nd order polynomial fit to weighted data
    gauss1.plotOn(yframe) ;


    // ------------------------------------ ZZ FIT ---------------------------------------------------------------------
    name = "jjfit";
    RooRealVar mean2(name+"_mean","mean of gaussian",1,-10,10);
    RooRealVar sigma2(name+"_sigma","width of gaussian",1,0.1,10);
    RooGaussian gauss2(name+"_gauss","gaussian PDF",z,mean,sigma) ;

    RooFitResult* zz_fit = gauss2.fitTo(*zz,Save()) ;
    RooFitResult* zz_fit_corr = gauss2.fitTo(*zz,Save(),SumW2Error(kTRUE)) ;

    RooPlot* zframe = z.frame(Title("Unbinned ML fit, binned chi^2 fit to weighted data")) ;

    // Plot data using sum-of-weights-squared error rather than Poisson errors
    (*zz).plotOn(zframe,DataError(RooAbsData::SumW2)) ;

    // Overlay result of 2nd order polynomial fit to weighted data
    gauss2.plotOn(zframe) ;

    // --------------------------- CANVAS -----------------------------------------------------------




    x.frame(Title("Combined_x"));
    y.frame(Title("Combined_y"));
    z.frame(Title("Combined_z"));

    //total.plotOn(xframe,LineColor(kRed));
    //total.plotOn(yframe,LineColor(kOrange));
    //total.plotOn(zframe,LineColor(kGreen));
    //ZZ->plotOn(xframe,LineColor(kOrange));
    //Higgs->plotOn(xframe,LineColor(kRed));
    //jj->plotOn(xframe,LineColor(kBlack));


    // Draw all frames on a canvas
    //c->Divide(3);
    //c->cd(1) ;
    TCanvas* c1 = new TCanvas("c1","c1",1200,900);
    gPad->SetLeftMargin(0.15) ;
    xframe->Draw() ;

    //c->cd(2) ;
    TCanvas* c2 = new TCanvas("c2","c2",1200,900);
    gPad->SetLeftMargin(0.15) ;
    c2->SetGridx(10), c2->SetGridy(20);
    yframe->Draw() ;

    //c->cd(3) ;
    TCanvas* c3 = new TCanvas("c3","c3",1200,900);
    gPad->SetLeftMargin(0.15) ;
    c3->SetGridx(10), c3->SetGridy(20);
    zframe->Draw() ;

    c1->SaveAs("/home/thoresen/JetBrainsProjects/ClionProjects/play_with_roofit/plot1.eps");
    c2->SaveAs("/home/thoresen/JetBrainsProjects/ClionProjects/play_with_roofit/plot2.eps");
    c3->SaveAs("/home/thoresen/JetBrainsProjects/ClionProjects/play_with_roofit/plot3.eps");

    return 0;
}

